#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
###############################################################################
#   author: Tim Nies
#   license: GPL3 (you should have obtained a copy by downloading this package)
#   name: Berthon1993.py
#
#   Reproduction of some figures of

#   H. A. Berthon, W. A. Bubb, and P. W. Kuchel. 13C n.m.r.
#   isotopomer and computer-simulation studies of the non-oxidative pentose
#   phosphate pathway of human erythrocytes. Biochemical Journal,
#   296(2):379–387, Dec. 1993.'
#
#   using the Python package modelbase and the label model specified in
#   LabelPPPmodel_classic.py
###############################################################################
"""

import matplotlib.pyplot as plt
from LabelPPPmodel_classic import label_pppmodel
from modelbase.ode import Simulator

# %%############################################################################
# --------------------------------Fig. 2B--------------------------------------#
###############################################################################

# start concentrations

y0d = {
    "Ru5P": 1e-7,
    "Xu5P": 1e-7,
    "Rib5P": 1e-7,
    "G3P": 1e-7,
    "Sed7P": 1e-7,
    "Ery4P": 1e-7,
    "Fru6P": 1e-7,
    "DHAP": 1e-7,
    "Sed1_7P2": 1e-7,
    "Fru1_6P2": 30e-3,
    "Man6P": 1e-7,
    "Glc6P": 20e-3,
    "Glc1P": 1e-7,
}

# model call and generation of isotopomer start vector

y0 = label_pppmodel.generate_y0(y0d, label_positions={"Glc6P": 0})
y0d["Glc6P__100000"] = 30e-3
label_s = Simulator(label_pppmodel)
label_s.initialise(y0)
label_s.integrator.atol = 1e-15
label_s.integrator.etol = 1e-15
t, y = label_s.simulate(t_end=300 * 60, steps=1000)


plt.plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__100000") * 1000,
    label="Glc6P-C1",
)
plt.plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__001000") * 5 * 1000,
    label="Glc6P-C3",
)
plt.plot(
    label_s.get_time() / 60,
    label_s.get_variable("Ru5P__10000") * 5 * 1000,
    label="Rub5P-C1",
)
plt.xlim(0, 300)
plt.ylim(0, 30)
plt.xlabel("Time [min]")
plt.ylabel("Concentrations [mM]")
plt.legend()
plt.show()

# %%#############################################################################
# --------------------------------Fig. 3---------------------------------------#
################################################################################

# start concentrations

y0d = {
    "Ru5P": 1e-7,
    "Xu5P": 1e-7,
    "Rib5P": 1e-7,
    "G3P": 1e-7,
    "Sed7P": 1e-7,
    "Ery4P": 1e-7,
    "Fru6P": 5e-6,
    "DHAP": 5e-6,
    "Sed1_7P2": 1e-7,
    "Fru1_6P2": 28.5e-3,
    "Man6P": 1e-7,
    "Glc6P": 19e-3,
    "Glc1P": 1e-7,
}

y0 = label_pppmodel.generate_y0(y0d)
y0["Glc6P__111111"] = 28.5e-3


label_s = Simulator(label_pppmodel)
label_s.initialise(y0)
label_s.integrator.atol = 1e-15
label_s.integrator.etol = 1e-15

t, y = label_s.simulate(t_end=50000, steps=10000)


fig, ax = plt.subplots(3, 1, sharex=True, figsize=(10, 10))
# plot 3a

ax[0].plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__111111") * 1000,
    label="[1,2,3,4,5,6-$^{13}C$]-Glc6P",
)
ax[0].plot(
    label_s.get_time() / 60, label_s.get_variable("Glc6P__000000") * 1000, label="Glc6P"
)
ax[0].plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__111000") * 1000,
    label="[1,2,3-$^{13}C$]-Glc6P",
)
ax[0].set_xlim(0, 50000 / 60)
ax[0].set_ylabel("Concentrations [mM]", size=15)
ax[0].tick_params(axis="y", labelsize=15)
ax[0].legend()


# plot 3b

ax[1].plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__110000") * 1000,
    label="[1,2-$^{13}C$]-Glc6P",
)
ax[1].plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__001000") * 1000,
    label="[3-$^{13}C$]-Glc6P",
)
ax[1].plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__001111") * 1000,
    label="[3,4,5,6-$^{13}C$]-Glc6P",
)
ax[1].plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__000111") * 1000,
    label="[4,5,6-$^{13}C$]-Glc6P",
)
ax[1].plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__110111") * 1000,
    label="[1,2,4,5,6-$^{13}C$]-Glc6P",
)
ax[1].set_xlim(0, 50000 / 60)
ax[1].set_ylabel("Concentrations [mM]", size=15)
ax[1].tick_params(axis="y", labelsize=15)
ax[1].legend()


# plot 3c

ax[2].plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__100000") * 1000
    + label_s.get_variable("Glc6P__010000") * 1000,
    label="[1-$^{13}C$]-Glc6P+[2-$^{13}C$]-Glc6P",
)
ax[2].plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__011000") * 1000
    + label_s.get_variable("Glc6P__101000") * 1000,
    label="[2,3-$^{13}C$]-Glc6P+[1,3-$^{13}C$]-Glc6P",
)
ax[2].plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__011111") * 1000
    + label_s.get_variable("Glc6P__101111") * 1000,
    label="[2,3,4,5,6-$^{13}C$]-Glc6P+[1,3,4,5,6-$^{13}C$]-Glc6P",
)
ax[2].plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__100111") * 1000
    + label_s.get_variable("Glc6P__010111") * 1000,
    label="[1,4,5,6-$^{13}C$]-Glc6P+[2,4,5,6-$^{13}C$]-Glc6P",
)
ax[2].set_xlabel("Time [min]", size=15)
ax[2].set_ylabel("Concentrations [mM]", size=15)
ax[2].tick_params(axis="both", labelsize=15)
ax[2].set_xlim(0, 50000 / 60)
ax[2].legend()

plt.show()
# %%############################################################################
# --------------------------------Fig. 6--------------------------------------#
###############################################################################

# TA DEFICIT

label_s.clear_results()
label_s.update_parameter("TA_Et", 0)
t, y = label_s.simulate(t_end=50000, steps=10000)


fig, ax = plt.subplots(2, 1, sharex=True, figsize=(10, 10))

# plot 6a

ax[0].plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__111111") * 1000,
    label="[1,2,3,4,5,6-$^{13}C$]-Glc6P",
)
ax[0].plot(
    label_s.get_time() / 60, label_s.get_variable("Glc6P__000000") * 1000, label="Glc6"
)
ax[0].plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__111000") * 1000,
    label="[1,2,3-$^{13}C$]-Glc6P",
)
ax[0].set_xlim(0, 50000 / 60)
ax[0].set_ylabel("Concentrations [mM]", size=15)
ax[0].tick_params(axis="y", labelsize=15)
ax[0].legend()


# plot 6b

ax[1].plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__110000") * 1000,
    label="[1,2-$^{13}C$]-Glc6P",
)
ax[1].plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__001000") * 1000,
    label="[3-$^{13}C$]-Glc6P",
)
ax[1].plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__001111") * 1000,
    label="[3,4,5,6-$^{13}C$]-Glc6P",
)
ax[1].plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__000111") * 1000,
    label="[4,5,6-$^{13}C$]-Glc6P",
)
ax[1].plot(
    label_s.get_time() / 60,
    label_s.get_variable("Glc6P__110111") * 1000,
    label="[1,2,4,5,6-$^{13}C$]-Glc6P",
)
ax[1].set_xlim(-1, 50000 / 60)
ax[1].set_xlabel("Time [min]", size=15)
ax[1].set_ylabel("Concentrations [mM]", size=15)
ax[1].tick_params(axis="both", labelsize=15)
ax[1].legend()
plt.show()
