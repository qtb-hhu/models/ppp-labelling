#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
###############################################################################
#   author: Tim Nies
#   license: GPL3 (you should have obtained a copy by downloading this package)   
#   name: LabelPPPmodel.py
#
#   Reimplemention and expansion of the basic model contained in PPPmodel.py to a
#   labled model. This is an example for the facilities of modelbase implemented
#   in modelbase version 1.0
#   
#   Please note that it is now also possible to implement reversible reactions in
#   the label model without splitting it in the forward and backward part.
#   For more information please visit the modelbase documentation
###############################################################################
"""

from modelbase.ode import LabelModel
from parameter import parameter_Berthon

label_pppmodel = LabelModel(parameter_Berthon)


def ratelawAf(S, Stot, Ptot, k1, k2, k3, k4, Et):
    nom = k1 * k3 * S
    denom = k2 + k3 + k1 * Stot + k4 * Ptot
    v = Et * (nom / denom)
    return v


def ratelawAr(P, Stot, Ptot, k1, k2, k3, k4, Et):
    nom = k2 * k4 * P
    denom = k2 + k3 + k1 * Stot + k4 * Ptot
    v = Et * (nom / denom)
    return v


def ratelawBf(A, B, Atot, Btot, Ptot, Qtot, k1, k2, k3, k4, k5, k6, k7, k8, Et):
    nom = k1 * k3 * k5 * k7 * A * B
    denom = (
        k1 * k3 * (k6 + k7) * Atot
        + k5 * k7 * (k2 + k3) * Btot
        + k2 * k4 * (k6 + k7) * Ptot
        + k6 * k8 * (k2 + k3) * Qtot
        + k1 * k5 * (k3 + k7) * Atot * Btot
        + k4 * k8 * (k2 + k6) * Ptot * Qtot
        + k5 * k8 * (k2 + k3) * Btot * Qtot
        + k1 * k4 * (k6 + k7) * Atot * Ptot
    )
    v = Et * (nom / denom)
    return v


def ratelawBr(P, Q, Atot, Btot, Ptot, Qtot, k1, k2, k3, k4, k5, k6, k7, k8, Et):
    nom = k2 * k4 * k6 * k8 * P * Q
    denom = (
        k1 * k3 * (k6 + k7) * Atot
        + k5 * k7 * (k2 + k3) * Btot
        + k2 * k4 * (k6 + k7) * Ptot
        + k6 * k8 * (k2 + k3) * Qtot
        + k1 * k5 * (k3 + k7) * Atot * Btot
        + k4 * k8 * (k2 + k6) * Ptot * Qtot
        + k5 * k8 * (k2 + k3) * Btot * Qtot
        + k1 * k4 * (k6 + k7) * Atot * Ptot
    )
    v = Et * (nom / denom)
    return v


def ratelawCf(S, Stot, Ptot, Qtot, k1, k2, k3, k4, k5, k6, Et):
    nom = k1 * k3 * k5 * S
    denom = (
        (k2 + k3) * k5
        + k1 * (k3 + k5) * Stot
        + k2 * k4 * Ptot
        + (k2 + k3) * k6 * Qtot
        + k1 * k4 * Stot * Ptot
        + k4 * k6 * Ptot * Qtot
    )
    v = Et * (nom / denom)
    return v


def ratelawCr(P, Q, Stot, Ptot, Qtot, k1, k2, k3, k4, k5, k6, Et):
    nom = k2 * k4 * k6 * P * Q
    denom = (
        (k2 + k3) * k5
        + k1 * (k3 + k5) * Stot
        + k2 * k4 * Ptot
        + (k2 + k3) * k6 * Qtot
        + k1 * k4 * Stot * Ptot
        + k4 * k6 * Ptot * Qtot
    )
    v = Et * (nom / denom)
    return v


label_pppmodel.add_label_compounds(
    {
        "Ru5P": 5,
        "Xu5P": 5,
        "Rib5P": 5,
        "G3P": 3,
        "Sed7P": 7,
        "Ery4P": 4,
        "Fru6P": 6,
        "DHAP": 3,
        "Sed1_7P2": 7,
        "Fru1_6P2": 6,
        "Man6P": 6,
        "Glc6P": 6,
        "Glc1P": 6,
    }
)


label_pppmodel.add_labelmap_reaction(
    rate_name="Ru5PEf",
    function=ratelawAf,
    stoichiometry={"Ru5P": -1, "Xu5P": 1},
    labelmap=[0, 1, 2, 3, 4],
    modifiers=["Ru5P", "Xu5P"],
    parameters=["RU5PE_k1", "RU5PE_k2", "RU5PE_k3", "RU5PE_k4", "RU5PE_Et"],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="Ru5PEr",
    function=ratelawAr,
    stoichiometry={"Xu5P": -1, "Ru5P": 1},
    labelmap=[0, 1, 2, 3, 4],
    modifiers=["Xu5P", "Ru5P"],
    parameters=["RU5PE_k1", "RU5PE_k2", "RU5PE_k3", "RU5PE_k4", "RU5PE_Et"],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="RIB5PIf",
    function=ratelawAf,
    stoichiometry={"Rib5P": -1, "Ru5P": 1},
    labelmap=[0, 1, 2, 3, 4],
    modifiers=["Rib5P", "Ru5P"],
    parameters=["RIB5PI_k1", "RIB5PI_k2", "RIB5PI_k3", "RIB5PI_k4", "RIB5PI_Et"],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="RIB5PIr",
    function=ratelawAr,
    stoichiometry={"Ru5P": -1, "Rib5P": 1},
    labelmap=[0, 1, 2, 3, 4],
    modifiers=["Ru5P", "Rib5P"],
    parameters=["RIB5PI_k1", "RIB5PI_k2", "RIB5PI_k3", "RIB5PI_k4", "RIB5PI_Et"],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="TK1f",
    function=ratelawBf,
    stoichiometry={"Xu5P": -1, "Rib5P": -1, "G3P": 1, "Sed7P": 1},
    labelmap=[2, 3, 4, 0, 1, 5, 6, 7, 8, 9],
    modifiers=["Xu5P", "Rib5P", "G3P", "Sed7P"],
    parameters=[
        "TK1_k1",
        "TK1_k2",
        "TK1_k3",
        "TK1_k4",
        "TK1_k5",
        "TK1_k6",
        "TK1_k7",
        "TK1_k8",
        "TK1_Et",
    ],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="TK1r",
    function=ratelawBr,
    stoichiometry={"G3P": -1, "Sed7P": -1, "Xu5P": 1, "Rib5P": 1},
    labelmap=[3, 4, 0, 1, 2, 5, 6, 7, 8, 9],
    modifiers=["Xu5P", "Rib5P", "G3P", "Sed7P"],
    parameters=[
        "TK1_k1",
        "TK1_k2",
        "TK1_k3",
        "TK1_k4",
        "TK1_k5",
        "TK1_k6",
        "TK1_k7",
        "TK1_k8",
        "TK1_Et",
    ],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="TK2f",
    function=ratelawBf,
    stoichiometry={"Xu5P": -1, "Ery4P": -1, "G3P": 1, "Fru6P": 1},
    labelmap=[2, 3, 4, 0, 1, 5, 6, 7, 8],
    modifiers=["Xu5P", "Ery4P", "G3P", "Fru6P"],
    parameters=[
        "TK2_k1",
        "TK2_k2",
        "TK2_k3",
        "TK2_k4",
        "TK2_k5",
        "TK2_k6",
        "TK2_k7",
        "TK2_k8",
        "TK2_Et",
    ],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="TK2r",
    function=ratelawBr,
    stoichiometry={"G3P": -1, "Fru6P": -1, "Xu5P": 1, "Ery4P": 1},
    labelmap=[3, 4, 0, 1, 2, 5, 6, 7, 8],
    modifiers=["Xu5P", "Ery4P", "G3P", "Fru6P"],
    parameters=[
        "TK2_k1",
        "TK2_k2",
        "TK2_k3",
        "TK2_k4",
        "TK2_k5",
        "TK2_k6",
        "TK2_k7",
        "TK2_k8",
        "TK2_Et",
    ],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="TAf",
    function=ratelawBf,
    stoichiometry={"Sed7P": -1, "G3P": -1, "Ery4P": 1, "Fru6P": 1},
    labelmap=[3, 4, 5, 6, 0, 1, 2, 7, 8, 9],
    modifiers=["Sed7P", "G3P", "Ery4P", "Fru6P"],
    parameters=[
        "TA_k1",
        "TA_k2",
        "TA_k3",
        "TA_k4",
        "TA_k5",
        "TA_k6",
        "TA_k7",
        "TA_k8",
        "TA_Et",
    ],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="TAr",
    function=ratelawBr,
    stoichiometry={"Ery4P": -1, "Fru6P": -1, "Sed7P": 1, "G3P": 1},
    labelmap=[4, 5, 6, 0, 1, 2, 3, 7, 8, 9],
    modifiers=["Sed7P", "G3P", "Ery4P", "Fru6P"],
    parameters=[
        "TA_k1",
        "TA_k2",
        "TA_k3",
        "TA_k4",
        "TA_k5",
        "TA_k6",
        "TA_k7",
        "TA_k8",
        "TA_Et",
    ],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="TIMf",
    function=ratelawAf,
    stoichiometry={"G3P": -1, "DHAP": 1},
    labelmap=[2, 1, 0],
    modifiers=["G3P", "DHAP"],
    parameters=["TIM_k1", "TIM_k2", "TIM_k3", "TIM_k4", "TIM_Et"],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="TIMr",
    function=ratelawAr,
    stoichiometry={"DHAP": -1, "G3P": 1},
    labelmap=[2, 1, 0],
    modifiers=["G3P", "DHAP"],
    parameters=["TIM_k1", "TIM_k2", "TIM_k3", "TIM_k4", "TIM_Et"],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="AL1f",
    function=ratelawCf,
    stoichiometry={"Sed1_7P2": -1, "Ery4P": 1, "DHAP": 1},
    labelmap=[3, 4, 5, 6, 0, 1, 2],
    modifiers=["Sed1_7P2", "Ery4P", "DHAP"],
    parameters=["AL1_k1", "AL1_k2", "AL1_k3", "AL1_k4", "AL1_k5", "AL1_k6", "AL1_Et"],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="AL1r",
    function=ratelawCr,
    stoichiometry={"Ery4P": -1, "DHAP": -1, "Sed1_7P2": 1},
    labelmap=[4, 5, 6, 0, 1, 2, 3],
    modifiers=["Sed1_7P2", "Ery4P", "DHAP"],
    parameters=["AL1_k1", "AL1_k2", "AL1_k3", "AL1_k4", "AL1_k5", "AL1_k6", "AL1_Et"],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="AL2f",
    function=ratelawCf,
    stoichiometry={"Fru1_6P2": -1, "G3P": 1, "DHAP": 1},
    labelmap=[3, 4, 5, 0, 1, 2],
    modifiers=["Fru1_6P2", "G3P", "DHAP"],
    parameters=["AL2_k1", "AL2_k2", "AL2_k3", "AL2_k4", "AL2_k5", "AL2_k6", "AL2_Et"],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="AL2r",
    function=ratelawCr,
    stoichiometry={"G3P": -1, "DHAP": -1, "Fru1_6P2": 1},
    labelmap=[3, 4, 5, 0, 1, 2],
    modifiers=["Fru1_6P2", "G3P", "DHAP"],
    parameters=["AL2_k1", "AL2_k2", "AL2_k3", "AL2_k4", "AL2_k5", "AL2_k6", "AL2_Et"],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="MIf",
    function=ratelawAf,
    stoichiometry={"Man6P": -1, "Fru6P": 1},
    labelmap=[0, 1, 2, 3, 4, 5],
    modifiers=["Man6P", "Fru6P"],
    parameters=["MI_k1", "MI_k2", "MI_k3", "MI_k4", "MI_Et"],
)


label_pppmodel.add_labelmap_reaction(
    rate_name="MIr",
    function=ratelawAr,
    stoichiometry={"Fru6P": -1, "Man6P": 1},
    labelmap=[0, 1, 2, 3, 4, 5],
    modifiers=["Man6P", "Fru6P"],
    parameters=["MI_k1", "MI_k2", "MI_k3", "MI_k4", "MI_Et"],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="GPIf",
    function=ratelawAf,
    stoichiometry={"Fru6P": -1, "Glc6P": 1},
    labelmap=[0, 1, 2, 3, 4, 5],
    modifiers=["Fru6P", "Glc6P"],
    parameters=["GPI_k1", "GPI_k2", "GPI_k3", "GPI_k4", "GPI_Et"],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="GPIr",
    function=ratelawAr,
    stoichiometry={"Glc6P": -1, "Fru6P": 1},
    labelmap=[0, 1, 2, 3, 4, 5],
    modifiers=["Fru6P", "Glc6P"],
    parameters=["GPI_k1", "GPI_k2", "GPI_k3", "GPI_k4", "GPI_Et"],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="PMf",
    function=ratelawAf,
    stoichiometry={"Glc1P": -1, "Glc6P": 1},
    labelmap=[0, 1, 2, 3, 4, 5],
    modifiers=["Glc1P", "Glc6P"],
    parameters=["PM_k1", "PM_k2", "PM_k3", "PM_k4", "PM_Et"],
)

label_pppmodel.add_labelmap_reaction(
    rate_name="PMr",
    function=ratelawAr,
    stoichiometry={"Glc6P": -1, "Glc1P": 1},
    labelmap=[0, 1, 2, 3, 4, 5],
    modifiers=["Glc1P", "Glc6P"],
    parameters=["PM_k1", "PM_k2", "PM_k3", "PM_k4", "PM_Et"],
)
