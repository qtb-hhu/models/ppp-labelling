#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
###############################################################################
#   author: Tim Nies
#   license: GPL3 (you should have obtained a copy by downloading this package)   
#   name: PPPmodel.py
#
#   Implementation of the basic F-type non-oxidative pentose phosphate pathway 
#   model originally proposed by McIntyre et al. (1989).
###############################################################################
"""

# BASIC MODEL FROM MCINTYRE ET AL. (1989)


from modelbase.ode import Model
from parameter import parameter_McIntyre


def ratelawA(S, P, k1, k2, k3, k4, Et):
    nom = k1 * k3 * S - k2 * k4 * P
    denom = k2 + k3 + k1 * S + k4 * P
    v = Et * (nom / denom)
    return v


def ratelawB(A, B, P, Q, k1, k2, k3, k4, k5, k6, k7, k8, Et):
    nom = k1 * k3 * k5 * k7 * A * B - k2 * k4 * k6 * k8 * P * Q
    denom = (
        k1 * k3 * (k6 + k7) * A
        + k5 * k7 * (k2 + k3) * B
        + k2 * k4 * (k6 + k7) * P
        + k6 * k8 * (k2 + k3) * Q
        + k1 * k5 * (k3 + k7) * A * B
        + k4 * k8 * (k2 + k6) * P * Q
        + k5 * k8 * (k2 + k3) * B * Q
        + k1 * k4 * (k6 + k7) * A * P
    )
    v = Et * (nom / denom)
    return v


def ratelawC(S, P, Q, k1, k2, k3, k4, k5, k6, Et):
    nom = k1 * k3 * k5 * S - k2 * k4 * k6 * P * Q
    denom = (
        (k2 + k3) * k5
        + k1 * (k3 + k5) * S
        + k2 * k4 * P
        + (k2 + k3) * k6 * Q
        + k1 * k4 * S * P
        + k4 * k6 * P * Q
    )
    v = Et * (nom / denom)
    return v


pppmodel = Model(parameter_McIntyre)


pppmodel.add_compounds(
    [
        "Ru5P",
        "Xu5P",
        "Rib5P",
        "G3P",
        "Sed7P",
        "Ery4P",
        "Fru6P",
        "DHAP",
        "Sed1_7P2",
        "Fru1_6P2",
        "Man6P",
        "Glc6P",
        "Glc1P",
        "UDPGlc",
        "Gal1P",
        "UDPGal1P",
    ]
)

# Ribulose-5-Phosphate Epimerase
pppmodel.add_reaction(
    rate_name="Ru5PE",
    function=ratelawA,
    stoichiometry={"Ru5P": -1, "Xu5P": 1},
    parameters=["RU5PE_k1", "RU5PE_k2", "RU5PE_k3", "RU5PE_k4", "RU5PE_Et"],
    reversible=True,
)

# Ribose-5-Phosphate Isomerase
pppmodel.add_reaction(
    rate_name=" RIB5PI",
    function=ratelawA,
    stoichiometry={"Ru5P": 1, "Rib5P": -1},
    parameters=["RIB5PI_k1", "RIB5PI_k2", "RIB5PI_k3", "RIB5PI_k4", "RIB5PI_Et"],
    reversible=True,
)

# Transketolase 1
pppmodel.add_reaction(
    rate_name="TK1",
    function=ratelawB,
    stoichiometry={"Xu5P": -1, "Rib5P": -1, "G3P": 1, "Sed7P": 1},
    parameters=[
        "TK1_k1",
        "TK1_k2",
        "TK1_k3",
        "TK1_k4",
        "TK1_k5",
        "TK1_k6",
        "TK1_k7",
        "TK1_k8",
        "TK1_Et",
    ],
    reversible=True,
)

# Transketolase2
pppmodel.add_reaction(
    rate_name="TK2",
    function=ratelawB,
    stoichiometry={"Xu5P": -1, "Ery4P": -1, "G3P": 1, "Fru6P": 1},
    parameters=[
        "TK2_k1",
        "TK2_k2",
        "TK2_k3",
        "TK2_k4",
        "TK2_k5",
        "TK2_k6",
        "TK2_k7",
        "TK2_k8",
        "TK2_Et",
    ],
    reversible=True,
)

# Transaldolase
pppmodel.add_reaction(
    rate_name="TA",
    function=ratelawB,
    stoichiometry={"Sed7P": -1, "G3P": -1, "Ery4P": 1, "Fru6P": 1},
    parameters=[
        "TA_k1",
        "TA_k2",
        "TA_k3",
        "TA_k4",
        "TA_k5",
        "TA_k6",
        "TA_k7",
        "TA_k8",
        "TA_Et",
    ],
    reversible=True,
)

# Triosephosphateisomerase
pppmodel.add_reaction(
    rate_name="TIM",
    function=ratelawA,
    stoichiometry={"G3P": -1, "DHAP": 1},
    parameters=["TIM_k1", "TIM_k2", "TIM_k3", "TIM_k4", "TIM_Et"],
    reversible=True,
)

# Aldolase 1
pppmodel.add_reaction(
    rate_name="AL1",
    function=ratelawC,
    stoichiometry={"Sed1_7P2": -1, "Ery4P": 1, "DHAP": 1},
    parameters=["AL1_k1", "AL1_k2", "AL1_k3", "AL1_k4", "AL1_k5", "AL1_k6", "AL1_Et"],
    reversible=True,
)

# Aldolase 2
pppmodel.add_reaction(
    rate_name="AL2",
    function=ratelawC,
    stoichiometry={"Fru1_6P2": -1, "G3P": 1, "DHAP": 1},
    parameters=["AL2_k1", "AL2_k2", "AL2_k3", "AL2_k4", "AL2_k5", "AL2_k6", "AL2_Et"],
    reversible=True,
)

# Mannose-6-Phosphate isomerase
pppmodel.add_reaction(
    rate_name="MI",
    function=ratelawA,
    stoichiometry={"Man6P": -1, "Fru6P": 1},
    parameters=["MI_k1", "MI_k2", "MI_k3", "MI_k4", "MI_Et"],
    reversible=True,
)

# Glucose-6-Phoshate isomerase
pppmodel.add_reaction(
    rate_name="GPI",
    function=ratelawA,
    stoichiometry={"Fru6P": -1, "Glc6P": 1},
    parameters=["GPI_k1", "GPI_k2", "GPI_k3", "GPI_k4", "GPI_Et"],
    reversible=True,
)

# Phosphoglucomutase
pppmodel.add_reaction(
    rate_name="PM",
    function=ratelawA,
    stoichiometry={"Glc1P": -1, "Glc6P": 1},
    parameters=["PM_k1", "PM_k2", "PM_k3", "PM_k4", "PM_Et"],
    reversible=True,
)

# Gal1-uridlytransferase
pppmodel.add_reaction(
    rate_name="UDPGal1PT",
    function=ratelawB,
    stoichiometry={"UDPGlc": -1, "Glc1P": -1, "Gal1P": 1, "UDPGal1P": 1},
    parameters=[
        "UDPGal1PT_k1",
        "UDPGal1PT_k2",
        "UDPGal1PT_k3",
        "UDPGal1PT_k4",
        "UDPGal1PT_k5",
        "UDPGal1PT_k6",
        "UDPGal1PT_k7",
        "UDPGal1PT_k8",
        "UDPGal1PT_Et",
    ],
    reversible=True,
)
