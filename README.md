# pppmodel

pppmodel is a reimplementation of a model orginally proposed by McIntyre et al. (1989) describing the F-type non-oxidative pentose phosphate pathway in erythrocytes. Later this model was adapted by Berthon et al. (1993) for the in silico replication of NMR studies.

This repository is an example of the Python package "modelbase" version 1.0 for simulating ordinary differential equations in metabolic networks. It is shown that the "modelbase" is capable of inferring the dynamics of labels in metabolic pathways (here the non-oxidative pentose phosphate pathway).

The reimplementation of the basic model and its expansion to a system that is able to simulate the dynamics of labels using the Python package "modelbase" is shown in the file "LabelPPPmodel_classic.py".

The file "Berthon1993.py" reproduces some figures of Berthon et al. (1993).

Please execute the file "Berthon1993.py" after  installing the Python package modelbase version 1.0 and all its dependencies.
 

## References

H. A. Berthon, W. A. Bubb, and P. W. Kuchel. '$^13$'C n.m.r. isotopomer and
computer-simulation studies of the non-oxidative pentose phosphate pathway of
human erythrocytes. Biochemical Journal, 296(2):379–387, Dec. 1993.

L. M. Mcintyre, D. R. Thorburn, W. A. Bubb, and P. W. Kuchel. Comparison of
computer simulations of the F-type and L-type non-oxidative hexose monophos-
phate shunts with 31p-NMR experimental data from human erythrocytes. Euro-
pean Journal of Biochemistry, 180(2):399–420, Mar. 1989.

